#!/bin/bash
# Fair License (Fair)
# Copyright (c) 2021 thomas.zink_(at)_uni-konstanz_(dot)_de
# Usage of the works is permitted provided that this instrument is retained with
# the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
# [https://opensource.org/licenses/Fair/](https://opensource.org/licenses/Fair/)

LOG=${1:-"/var/log"}

declare -A months=( [01]="Jan" [02]="Feb" [03]="Mar" [04]="Apr" [05]="May" [06]="Jun" [07]="Jul" [08]="Aug" [09]="Sep" [10]="Okt" [11]="Nov" [12]="Dec" )

while read -r line; do
    ip=$(echo "$line" | cut -d' ' -f1)
    mond=$(echo "$line" | cut -d'-' -f2)
    mons=${months["$mond"]}
    grep -r "$ip" "${LOG}"/* | grep -E "\-$mond|$mons\ "
    find "${LOG}/" -name "*.gz" -exec zgrep "$ip" {} \; | grep -E "\-$mond|$mons\ "
done < apt31-indicators.txt

